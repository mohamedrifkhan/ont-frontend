import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule,NgbPaginationModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LoginComponent } from './login/Components/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartModule } from "angular-highcharts";
import { ChartsComponent } from "./statistics/Components/charts/charts.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ChartsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NgbPaginationModule, 
    NgbAlertModule, 
    FontAwesomeModule, 
    BrowserAnimationsModule,
    ChartModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
