import { Injectable, OnInit } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavbarShowHideService implements OnInit{

  constructor() { }
  visible=false;
  show(){
    this.visible=true;
  }
  hide(){
    this.visible=false;
  }

  ngOnInit(): void {
    this.visible=true;
  }
}
