import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiomonitoringDonorsComponent } from './biomonitoring-donors.component';

describe('BiomonitoringDonorsComponent', () => {
  let component: BiomonitoringDonorsComponent;
  let fixture: ComponentFixture<BiomonitoringDonorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BiomonitoringDonorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BiomonitoringDonorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
