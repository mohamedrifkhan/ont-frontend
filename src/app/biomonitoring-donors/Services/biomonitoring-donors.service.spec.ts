import { TestBed } from '@angular/core/testing';

import { BiomonitoringDonorsService } from './biomonitoring-donors.service';

describe('BiomonitoringDonorsService', () => {
  let service: BiomonitoringDonorsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BiomonitoringDonorsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
