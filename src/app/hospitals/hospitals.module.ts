import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HospitalsComponent } from './Components/hospitals/hospitals.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterialExampleModule } from "./../../material.module";
import { MatNativeDateModule } from '@angular/material/core';

const routes: Routes= [
  {
    path:'',
    component:HospitalsComponent
  }
]

@NgModule({
  declarations: [
    HospitalsComponent
  ],
  imports: [
    CommonModule,
    [RouterModule.forChild(routes)],
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialExampleModule,
    MatNativeDateModule
  ]
})
export class HospitalsModule { }
