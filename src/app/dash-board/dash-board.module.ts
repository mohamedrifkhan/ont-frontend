import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashBoardComponent } from './Components/dash-board/dash-board.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



const routes: Routes= [
  {
    path:'',
    component:DashBoardComponent
  }
]

@NgModule({
  declarations: [
    DashBoardComponent
  ],
  imports: [
    CommonModule,
    [RouterModule.forChild(routes)],
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class DashBoardModule { }
