import { Component, OnInit } from '@angular/core';
import { NavbarShowHideService } from 'src/app/_services/navbar-show-hide.service';

@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.component.html',
  styleUrls: ['./dash-board.component.css']
})
export class DashBoardComponent implements OnInit {

  constructor(private nav:NavbarShowHideService) { 
    this.nav.show();
  }

  ngOnInit(): void {
  }

  donares:{name:string,count:number}[]=[
    {
    "name":"Neoplasias",
    "count":100
  },
  {
    "name":"Tóxicos",
    "count":100
  },
  {
    "name":"Infecciones",
    "count":100
  },
  {
    "name":"Otros",
    "count":100
  }
]

rows:{name:string}[]=[
  {
    "name":"corazón"
  },
  {
    "name":"páncreas"
  },
  {
    "name":"pulmón"
  },
  {
    "name":"hígado"
  },
  {
    "name":"riñón"
  }
]

}
