import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/Components/login/login.component';
import { ChartsComponent } from "./statistics/Components/charts/charts.component";

const routes: Routes = [
  {path:"login",component:LoginComponent},
  {path:"chart",component:ChartsComponent},
  {path:"dashboard",loadChildren:()=>import('./dash-board/dash-board.module').then(m=>m.DashBoardModule)},
  {path:"status",loadChildren:()=>import('./status/status.module').then(m=>m.StatusModule)},
  {path:"statistics",loadChildren:()=>import('./statistics/statistics.module').then(m=>m.StatisticsModule)},
  {path:"types-of-tracking",loadChildren:()=>import('./types-of-tracking/types-of-tracking.module').then(m=>m.TypesOfTrackingModule)},
  {path:"coordinator-management",loadChildren:()=>import('./coordinator-management/coordinator-management.module').then(m=>m.CoordinatorManagementModule)},
  {path:"management-of-transplant-equipment",loadChildren:()=>import('./management-of-transplant-equipment/management-of-transplant-equipment.module').then(m=>m.ManagementOfTransplantEquipmentModule)},
  {path:"hospitals",loadChildren:()=>import('./hospitals/hospitals.module').then(m=>m.HospitalsModule)},
  {path:"receivers",loadChildren:()=>import('./receivers/receivers.module').then(m=>m.ReceiversModule)},
  {path:"incidents",loadChildren:()=>import('./incidents/incidents.module').then(m=>m.IncidentsModule)},
  {path:"notifications",loadChildren:()=>import('./biosurveillance-notifications/biosurveillance-notifications.module').then(m=>m.BiosurveillanceNotificationsModule)},
  {path:"biomonitoring-donors",loadChildren:()=>import('./biomonitoring-donors/biomonitoring-donors.module').then(m=>m.BiomonitoringDonorsModule)},
  {path:"biomonitoring-receptors",loadChildren:()=>import('./biomonitoring-receptors/biomonitoring-receptors.module').then(m=>m.BiomonitoringReceptorsModule)},
  {path:"alta-biovigilancia",loadChildren:()=>import('./alta-biovigilancia/alta-biovigilancia.module').then(m=>m.AltaBiovigilanciaModule)},
  {path:'',redirectTo:'login',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
