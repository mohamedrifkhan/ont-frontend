import { AfterViewInit, Component, OnInit } from '@angular/core';
import { NavbarShowHideService } from './_services/navbar-show-hide.service';
import { faExclamationTriangle,faWrench,faTh,faUser } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'ont-frontend';
  faExclamationTriangle = faExclamationTriangle;
  faWrench=faWrench;
  faTh=faTh;
  faUser=faUser;
  is_control=true;
  is_manage=false;
  is_bio=false;
  constructor(public nav:NavbarShowHideService){
    
  }


  touch_control(){
    this.is_control=true;
    this.is_manage=false;
    this.is_bio=false;
  }

  touch_manage(){
    this.is_control=false;
    this.is_manage=true;
    this.is_bio=false;
  }

  touch_bio(){
    this.is_control=false;
    this.is_manage=false;
    this.is_bio=true;
  }
}
