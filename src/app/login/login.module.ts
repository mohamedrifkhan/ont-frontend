import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './Components/login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoadingBarComponent } from './Components/loading-bar/loading-bar.component';

const routes: Routes= [
  {
    path:'',
    component:LoginComponent
  }
]

@NgModule({
  declarations: [
    // LoginComponent,
    LoadingBarComponent
  ],
  imports: [
    CommonModule,
    [RouterModule.forChild(routes)],
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class LoginModule { }
