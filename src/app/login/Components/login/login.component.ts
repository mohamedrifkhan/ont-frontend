import { AfterViewChecked, AfterViewInit, Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { NavbarShowHideService } from 'src/app/_services/navbar-show-hide.service';
import { LoadingBarComponent } from '../loading-bar/loading-bar.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login_page=true;
  // loading_page=false;

  constructor(private route:Router,private nav:NavbarShowHideService) { 
    this.nav.hide();
  }

  ngOnInit(): void {

  }

  toLogin(){
    this.login_page=false;
    
    // this.loading_page=true;
    var elem = document.getElementById("loadingBar")!
    document.getElementById("init-splash")?.removeAttribute('hidden');
    var width = 1;
    var id = setInterval(() => {
      if (width >= 100) {
        clearInterval(id);
        this.route.navigate(['dashboard']);
      } else {
        width++;
        elem.style.width = width + '%';
      }
    }, 10);
  }

}
