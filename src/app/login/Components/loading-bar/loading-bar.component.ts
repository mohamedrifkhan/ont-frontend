import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading-bar',
  templateUrl: './loading-bar.component.html',
  styleUrls: ['./loading-bar.component.css']
})
export class LoadingBarComponent implements OnInit {

  
  loading_bar:any;
  constructor() { }

  ngOnInit(): void {
    this.loading_bar=document.getElementById("loading-bar");
  }

}
