import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BiomonitoringReceptorsComponent } from './Components/biomonitoring-receptors/biomonitoring-receptors.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialExampleModule } from 'src/material.module';
import { MatNativeDateModule } from '@angular/material/core';


const routes: Routes= [
  {
    path:'',
    component:BiomonitoringReceptorsComponent
  }
]

@NgModule({
  declarations: [
    BiomonitoringReceptorsComponent
  ],
  imports: [
    CommonModule,
    [RouterModule.forChild(routes)],
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialExampleModule,
    MatNativeDateModule
  ]
})
export class BiomonitoringReceptorsModule { }
