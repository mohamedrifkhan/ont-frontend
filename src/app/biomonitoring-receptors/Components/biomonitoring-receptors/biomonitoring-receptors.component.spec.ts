import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiomonitoringReceptorsComponent } from './biomonitoring-receptors.component';

describe('BiomonitoringReceptorsComponent', () => {
  let component: BiomonitoringReceptorsComponent;
  let fixture: ComponentFixture<BiomonitoringReceptorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BiomonitoringReceptorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BiomonitoringReceptorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
