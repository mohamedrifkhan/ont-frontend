import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { NavbarShowHideService } from 'src/app/_services/navbar-show-hide.service';

export interface UserData {
  id: string;
  name: string;
  progress: string;
  fruit: string;
}

const FRUITS: string[] = [
  'blueberry',
  'lychee',
  'kiwi',
  'mango',
  'peach',
  'lime',
  'pomegranate',
  'pineapple',
];
const NAMES: string[] = [
  'Maia',
  'Asher',
  'Olivia',
  'Atticus',
  'Amelia',
  'Jack',
  'Charlotte',
  'Theodore',
  'Isla',
  'Oliver',
  'Isabella',
  'Jasper',
  'Cora',
  'Levi',
  'Violet',
  'Arthur',
  'Mia',
  'Thomas',
  'Elizabeth',
];
@Component({
  selector: 'app-biomonitoring-receptors',
  templateUrl: './biomonitoring-receptors.component.html',
  styleUrls: ['./biomonitoring-receptors.component.css']
})
export class BiomonitoringReceptorsComponent implements OnInit,AfterViewInit{
  displayedColumns: string[] = ['id', 'name', 'progress', 'fruit','name1','fruit1'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator)paginator!: MatPaginator;
  @ViewChild(MatSort)sort!: MatSort;
  title = 'pagination';
  navBar="Abiertos";
  server_api:boolean | undefined;
  paginated:boolean | undefined;
  loading:boolean | undefined;
  constructor(private nav:NavbarShowHideService){
    this.nav.show();


    if(this.navBar=="Cerrados"||this.navBar=="Desestimades"){
      this.dataSource=new MatTableDataSource<UserData>();
    }
    else{
      const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));
      this.dataSource = new MatTableDataSource(users);
    }
  }

  ngOnInit(): void {
    this.server_api=true;
    this.paginated=true;
    this.loading=false;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  onTable(value:any){
    this.navBar=value;
    if(this.navBar=="Cerrados"||this.navBar=="Desestimades"){
      this.dataSource=new MatTableDataSource();
      this.dataSource.paginator;
      this.dataSource.sort;
    }
    else{
      const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));
      this.dataSource = new MatTableDataSource(users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
}

function createNewUser(id: number): UserData {
  const name =
    NAMES[Math.round(Math.random() * (NAMES.length - 1))] +
    ' ' +
    NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) +
    '.';

  return {
    id: id.toString(),
    name: name,
    progress: Math.round(Math.random() * 100).toString(),
    fruit: FRUITS[Math.round(Math.random() * (FRUITS.length - 1))],
  };
}

