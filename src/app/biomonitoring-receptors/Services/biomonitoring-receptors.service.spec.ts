import { TestBed } from '@angular/core/testing';

import { BiomonitoringReceptorsService } from './biomonitoring-receptors.service';

describe('BiomonitoringReceptorsService', () => {
  let service: BiomonitoringReceptorsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BiomonitoringReceptorsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
