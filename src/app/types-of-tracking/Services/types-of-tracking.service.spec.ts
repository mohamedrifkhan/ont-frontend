import { TestBed } from '@angular/core/testing';

import { TypesOfTrackingService } from './types-of-tracking.service';

describe('TypesOfTrackingService', () => {
  let service: TypesOfTrackingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TypesOfTrackingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
