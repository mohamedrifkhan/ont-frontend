import { TestBed } from '@angular/core/testing';

import { NewTypeTrackingService } from './new-type-tracking.service';

describe('NewTypeTrackingService', () => {
  let service: NewTypeTrackingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NewTypeTrackingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
