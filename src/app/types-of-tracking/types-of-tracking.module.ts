import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TypesOfTrackingComponent } from './Components/types-of-tracking/types-of-tracking.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialExampleModule } from 'src/material.module';
import { MatNativeDateModule } from '@angular/material/core';
import { NewTypeTrackingComponent } from './Components/new-type-tracking/new-type-tracking.component';



const routes: Routes= [
  {
    path:'',
    component:TypesOfTrackingComponent
  },
  {
    path:'new',
    component:NewTypeTrackingComponent
  }
]

@NgModule({
  declarations: [
    TypesOfTrackingComponent,
    NewTypeTrackingComponent
  ],
  imports: [
    CommonModule,
    [RouterModule.forChild(routes)],
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialExampleModule,
    MatNativeDateModule
  ]
})
export class TypesOfTrackingModule { }
