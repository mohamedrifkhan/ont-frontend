import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewTypeTrackingComponent } from './new-type-tracking.component';

describe('NewTypeTrackingComponent', () => {
  let component: NewTypeTrackingComponent;
  let fixture: ComponentFixture<NewTypeTrackingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewTypeTrackingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTypeTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
