import { Component, OnInit } from '@angular/core';
import { NavbarShowHideService } from 'src/app/_services/navbar-show-hide.service';

export class Questions{
  id:any;
  name:any;
  url:any;
  type:any;
}

@Component({
  selector: 'app-new-type-tracking',
  templateUrl: './new-type-tracking.component.html',
  styleUrls: ['./new-type-tracking.component.css']
})
export class NewTypeTrackingComponent implements OnInit {
  isTitle=false;
  title="";
  type="";
  url="";
  values:any=[];
  selectedService:any;
  // types:any=['texto','decimal','selector','fecha','hora','si-no','general'];
  questions=new Questions;
  constructor(private nav:NavbarShowHideService) { 
    this.nav.show();
  }

  ngOnInit(): void {
  }

  onType(value:any){
    this.type=value;
    this.url="./../../../../assets/img/form-";
    this.url=this.url+value+".png";
    this.questions=new Questions;
    this.questions.url=this.url;
    this.questions.type=this.type;
    this.values.push(this.questions);

    console.log(this.questions);
    
  }
  
}
