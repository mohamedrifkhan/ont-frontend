import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypesOfTrackingComponent } from './types-of-tracking.component';

describe('TypesOfTrackingComponent', () => {
  let component: TypesOfTrackingComponent;
  let fixture: ComponentFixture<TypesOfTrackingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypesOfTrackingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypesOfTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
