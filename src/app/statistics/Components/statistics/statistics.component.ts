import { Component, OnInit } from '@angular/core';
import { NavbarShowHideService } from 'src/app/_services/navbar-show-hide.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {

  constructor(private nav:NavbarShowHideService) { 
    this.nav.show();
  }

  ngOnInit(): void {
  }

}
