import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { NavbarShowHideService } from 'src/app/_services/navbar-show-hide.service';
import { pieChartOption } from "./../../../_helpers/pieChartOption";

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {
  pieChart = new Chart(pieChartOption);
  constructor(private nav:NavbarShowHideService) {
   }

  ngOnInit(): void {
    this.nav.hide();
  }

}
