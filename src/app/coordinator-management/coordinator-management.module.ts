import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoordinatorManagementComponent } from './Components/coordinator-management/coordinator-management.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialExampleModule } from 'src/material.module';
import { MatNativeDateModule } from '@angular/material/core';
import { NewCoordinatorManagementComponent } from './Components/new-coordinator-management/new-coordinator-management.component';



const routes: Routes= [
  {
    path:'',
    component:CoordinatorManagementComponent
  },
  {
    path:'new',
    component:NewCoordinatorManagementComponent
  }
]

@NgModule({
  declarations: [
    CoordinatorManagementComponent,
    NewCoordinatorManagementComponent
  ],
  imports: [
    CommonModule,
    [RouterModule.forChild(routes)],
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialExampleModule,
    MatNativeDateModule
  ]
})
export class CoordinatorManagementModule { }
