import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCoordinatorManagementComponent } from './new-coordinator-management.component';

describe('NewCoordinatorManagementComponent', () => {
  let component: NewCoordinatorManagementComponent;
  let fixture: ComponentFixture<NewCoordinatorManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewCoordinatorManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCoordinatorManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
