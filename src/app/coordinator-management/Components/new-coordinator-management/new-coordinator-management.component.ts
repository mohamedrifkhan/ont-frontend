import { Component, OnInit } from '@angular/core';
import { NavbarShowHideService } from 'src/app/_services/navbar-show-hide.service';

@Component({
  selector: 'app-new-coordinator-management',
  templateUrl: './new-coordinator-management.component.html',
  styleUrls: ['./new-coordinator-management.component.css']
})
export class NewCoordinatorManagementComponent implements OnInit {

  user_type="Coordinator ONT";
  regions=["abc","def","ghi"];
  h_map=["abc","def","ghi"];
  ttxs=["abc","def","ghi"];
  constructor(private nav:NavbarShowHideService) { 
    this.nav.show();
  }

  ngOnInit(): void {
  }

}
