import { TestBed } from '@angular/core/testing';

import { CoordinatorManagementService } from './coordinator-management.service';

describe('CoordinatorManagementService', () => {
  let service: CoordinatorManagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CoordinatorManagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
