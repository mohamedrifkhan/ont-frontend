import { TestBed } from '@angular/core/testing';

import { NewCoordinatorManagementService } from './new-coordinator-management.service';

describe('NewCoordinatorManagementService', () => {
  let service: NewCoordinatorManagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NewCoordinatorManagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
