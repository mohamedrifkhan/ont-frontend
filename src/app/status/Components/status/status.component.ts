import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { NavbarShowHideService } from 'src/app/_services/navbar-show-hide.service';

export interface UserData {
  id: string;
  name: string;
  progress: string;
  fruit: string;
}

const FRUITS: string[] = [
  'blueberry',
  'lychee',
  'kiwi',
  'mango',
  'peach',
  'lime',
  'pomegranate',
  'pineapple',
];
const NAMES: string[] = [
  'Maia',
  'Asher',
  'Olivia',
  'Atticus',
  'Amelia',
  'Jack',
  'Charlotte',
  'Theodore',
  'Isla',
  'Oliver',
  'Isabella',
  'Jasper',
  'Cora',
  'Levi',
  'Violet',
  'Arthur',
  'Mia',
  'Thomas',
  'Elizabeth',
];
@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class StatusComponent implements OnInit,AfterViewInit{
  displayedColumns: string[] = ['id', 'name', 'progress', 'fruit','id1','progress1','name1','fruit1','name2','fruit2','fruit3','delete'];
  displayedColumnsClose: string[] = ['id', 'name', 'progress', 'fruit','id1','progress1','name1','fruit1','name2','fruit2','fruit3','fruit4','delete'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator)paginator!: MatPaginator;
  @ViewChild(MatSort)sort!: MatSort;
  title = 'pagination';
  open=true;
  close=false;
  incidence=false;
  order=false;
  server_api:boolean | undefined;
  paginated:boolean | undefined;
  loading:boolean | undefined;
  constructor(private nav:NavbarShowHideService){
    const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));
    this.nav.show();
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(users);
  }

  ngOnInit(): void {
    this.server_api=true;
    this.paginated=true;
    this.loading=false;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onNav(value:any){
    if(value=="open"){
      this.open=true;
      this.close=false;
      this.incidence=false;
      this.order=false;
    }
    else if(value=="close"){
      this.open=false;
      this.close=true;
      this.incidence=false;
      this.order=false;      
      console.log(this.close);
      console.log(this.open);
      
    }
    else if(value=="incidence"){
      this.open=false;
      this.close=false;
      this.incidence=true;
      this.order=false;
    }
    else{
      this.open=false;
      this.close=false;
      this.incidence=false;
      this.order=true;
    }
  }
  
}

function createNewUser(id: number): UserData {
  const name =
    NAMES[Math.round(Math.random() * (NAMES.length - 1))] +
    ' ' +
    NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) +
    '.';

  return {
    id: id.toString(),
    name: name,
    progress: Math.round(Math.random() * 100).toString(),
    fruit: FRUITS[Math.round(Math.random() * (FRUITS.length - 1))],
  };
}

