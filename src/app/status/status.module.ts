import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusComponent } from './Components/status/status.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterialExampleModule } from "./../../material.module";
import { MatNativeDateModule } from '@angular/material/core';

const routes: Routes= [
  {
    path:'',
    component:StatusComponent
  }
]

@NgModule({
  declarations: [
    StatusComponent
  ],
  imports: [
    CommonModule,
    [RouterModule.forChild(routes)],
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialExampleModule,
    MatNativeDateModule
  ]
})
export class StatusModule { }
