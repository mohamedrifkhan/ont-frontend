import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NavbarShowHideService } from 'src/app/_services/navbar-show-hide.service';

@Component({
  selector: 'app-alta-biovigilancia',
  templateUrl: './alta-biovigilancia.component.html',
  styleUrls: ['./alta-biovigilancia.component.css']
})
export class AltaBiovigilanciaComponent implements OnInit {
role='ont';
ccaas=['ab','bc','cd'];
hospital=['ssf','sffx','eijr'];
forEvento=false;
forReaccion=false;

NotificationForm=new FormGroup({
  formAlerta:new FormControl('')
})
  constructor(private nav:NavbarShowHideService) { 
    this.nav.show();
  }

  ngOnInit(): void {
  }
  alerta(){
    if(this.NotificationForm.get('formAlerta')?.value=="evento"){
      this.forEvento=true;
      this.forReaccion=false;
    }
    if(this.NotificationForm.get('formAlerta')?.value=="reaccion"){
      this.forEvento=false;
      this.forReaccion=true;
    }
    if(this.NotificationForm.get('formAlerta')?.value==""){
      this.forEvento=false;
      this.forReaccion=false;
    }
  }
}
