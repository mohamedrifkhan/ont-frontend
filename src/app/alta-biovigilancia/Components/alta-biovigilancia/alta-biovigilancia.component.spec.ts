import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaBiovigilanciaComponent } from './alta-biovigilancia.component';

describe('AltaBiovigilanciaComponent', () => {
  let component: AltaBiovigilanciaComponent;
  let fixture: ComponentFixture<AltaBiovigilanciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AltaBiovigilanciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaBiovigilanciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
