import { TestBed } from '@angular/core/testing';

import { AltaBiovigilanciaService } from './alta-biovigilancia.service';

describe('AltaBiovigilanciaService', () => {
  let service: AltaBiovigilanciaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AltaBiovigilanciaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
