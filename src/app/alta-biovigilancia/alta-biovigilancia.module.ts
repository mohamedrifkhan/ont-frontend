import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AltaBiovigilanciaComponent } from './Components/alta-biovigilancia/alta-biovigilancia.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialExampleModule } from 'src/material.module';
import { MatNativeDateModule } from '@angular/material/core';


const routes: Routes= [
  {
    path:'',
    component:AltaBiovigilanciaComponent
  }
]

@NgModule({
  declarations: [
    AltaBiovigilanciaComponent
  ],
  imports: [
    CommonModule,
    [RouterModule.forChild(routes)],
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialExampleModule,
    MatNativeDateModule
  ]
})
export class AltaBiovigilanciaModule { }
