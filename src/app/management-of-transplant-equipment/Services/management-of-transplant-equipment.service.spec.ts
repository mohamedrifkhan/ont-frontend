import { TestBed } from '@angular/core/testing';

import { ManagementOfTransplantEquipmentService } from './management-of-transplant-equipment.service';

describe('ManagementOfTransplantEquipmentService', () => {
  let service: ManagementOfTransplantEquipmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ManagementOfTransplantEquipmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
