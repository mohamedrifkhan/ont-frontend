import { TestBed } from '@angular/core/testing';

import { NewManagementEquipmentServiceService } from './new-management-equipment-service.service';

describe('NewManagementEquipmentServiceService', () => {
  let service: NewManagementEquipmentServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NewManagementEquipmentServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
