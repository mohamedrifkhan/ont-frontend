import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewManagementEquipmentComponent } from './new-management-equipment.component';

describe('NewManagementEquipmentComponent', () => {
  let component: NewManagementEquipmentComponent;
  let fixture: ComponentFixture<NewManagementEquipmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewManagementEquipmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewManagementEquipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
