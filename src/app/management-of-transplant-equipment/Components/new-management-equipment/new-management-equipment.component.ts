import { Component, OnInit } from '@angular/core';
import { NavbarShowHideService } from 'src/app/_services/navbar-show-hide.service';

@Component({
  selector: 'app-new-management-equipment',
  templateUrl: './new-management-equipment.component.html',
  styleUrls: ['./new-management-equipment.component.css']
})
export class NewManagementEquipmentComponent implements OnInit {

  user_type="equipos";
  h_map=["1","2"];
  ttxs=["1","2","3"];
  regions=["a","b","c"]
  constructor(private nav:NavbarShowHideService) {
    this.nav.show();
   }

  ngOnInit(): void {
  }

}
