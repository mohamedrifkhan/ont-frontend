import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementOfTransplantEquipmentComponent } from './management-of-transplant-equipment.component';

describe('ManagementOfTransplantEquipmentComponent', () => {
  let component: ManagementOfTransplantEquipmentComponent;
  let fixture: ComponentFixture<ManagementOfTransplantEquipmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementOfTransplantEquipmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementOfTransplantEquipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
