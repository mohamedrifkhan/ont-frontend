import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialExampleModule } from 'src/material.module';
import { MatNativeDateModule } from '@angular/material/core';
import { ManagementOfTransplantEquipmentComponent } from './Components/management-of-transplant-equipment/management-of-transplant-equipment.component';
import { NewManagementEquipmentComponent } from "./Components/new-management-equipment/new-management-equipment.component";


const routes: Routes= [
  {
    path:'',
    component:ManagementOfTransplantEquipmentComponent
  },
  {
    path:'new',
    component:NewManagementEquipmentComponent
  }
]

@NgModule({
  declarations: [
    ManagementOfTransplantEquipmentComponent,
    NewManagementEquipmentComponent
  ],
  imports: [
    CommonModule,
    [RouterModule.forChild(routes)],
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialExampleModule,
    MatNativeDateModule
  ]
})
export class ManagementOfTransplantEquipmentModule { }

