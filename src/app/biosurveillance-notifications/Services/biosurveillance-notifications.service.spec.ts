import { TestBed } from '@angular/core/testing';

import { BiosurveillanceNotificationsService } from './biosurveillance-notifications.service';

describe('BiosurveillanceNotificationsService', () => {
  let service: BiosurveillanceNotificationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BiosurveillanceNotificationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
