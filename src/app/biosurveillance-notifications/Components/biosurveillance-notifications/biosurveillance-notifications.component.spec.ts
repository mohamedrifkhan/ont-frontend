import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiosurveillanceNotificationsComponent } from './biosurveillance-notifications.component';

describe('BiosurveillanceNotificationsComponent', () => {
  let component: BiosurveillanceNotificationsComponent;
  let fixture: ComponentFixture<BiosurveillanceNotificationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BiosurveillanceNotificationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BiosurveillanceNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
