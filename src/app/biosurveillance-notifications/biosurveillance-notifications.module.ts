import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BiosurveillanceNotificationsComponent } from './Components/biosurveillance-notifications/biosurveillance-notifications.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialExampleModule } from 'src/material.module';
import { MatNativeDateModule } from '@angular/material/core';


const routes: Routes= [
  {
    path:'',
    component:BiosurveillanceNotificationsComponent
  }
]

@NgModule({
  declarations: [
    BiosurveillanceNotificationsComponent
  ],
  imports: [
    CommonModule,
    [RouterModule.forChild(routes)],
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialExampleModule,
    MatNativeDateModule
  ]
})
export class BiosurveillanceNotificationsModule { }
