import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { AfterViewChecked, AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NavbarShowHideService } from 'src/app/_services/navbar-show-hide.service';

export interface UserData {
  id: string;
  name: string;
  progress: string;
  fruit: string;
}

const FRUITS: string[] = [
  'blueberry',
  'lychee',
  'kiwi',
  'mango',
  'peach',
  'lime',
  'pomegranate',
  'pineapple',
];
const NAMES: string[] = [
  'Maia',
  'Asher',
  'Olivia',
  'Atticus',
  'Amelia',
  'Jack',
  'Charlotte',
  'Theodore',
  'Isla',
  'Oliver',
  'Isabella',
  'Jasper',
  'Cora',
  'Levi',
  'Violet',
  'Arthur',
  'Mia',
  'Thomas',
  'Elizabeth',
];

@Component({
  selector: 'app-incidents',
  templateUrl: './incidents.component.html',
  styleUrls: ['./incidents.component.css']
})
export class IncidentsComponent implements OnInit,AfterViewInit {

  displayedColumns: string[] = ['id', 'name', 'progress', 'fruit','delete'];
  displayedColumnsAutonomico: string[] = ['id', 'name', 'progress', 'fruit','delete'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator)paginator!: MatPaginator;
  @ViewChild(MatSort)sort!: MatSort;
  title = 'pagination';
  server_api:boolean | undefined;
  paginated:boolean | undefined;
  loading:boolean | undefined;
  user:any;
  constructor(private nav:NavbarShowHideService){
    this.user = Array.from({length: 100}, (_, k) => createNewUser(k + 1));
    this.nav.show();
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(this.user);
  }

  coordinator_type="autonomico";
  button_text="";
  ngOnInit(): void {
    this.onButtonText();
    this.server_api=true;
    this.paginated=true;
    this.loading=false;

  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  async onTable(value:any){
    this.coordinator_type=value;
    await this.onButtonText();
    // this.ngAfterViewInit();
  }

  onButtonText(){
    if(this.coordinator_type=="ont"){
      this.button_text="ONT";
    }
    if(this.coordinator_type=="autonomico"){
      this.button_text="Autonómico";
    }
    if(this.coordinator_type=="trasplantador"){
      this.button_text="de H. Generador";
    }
  }

}

function createNewUser(id: number): UserData {
  const name =
    NAMES[Math.round(Math.random() * (NAMES.length - 1))] +
    ' ' +
    NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) +
    '.';

  return {
    id: id.toString(),
    name: name,
    progress: Math.round(Math.random() * 100).toString(),
    fruit: FRUITS[Math.round(Math.random() * (FRUITS.length - 1))],
  };
}