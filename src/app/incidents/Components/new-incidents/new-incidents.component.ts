import { Component, OnInit } from '@angular/core';
import { NavbarShowHideService } from 'src/app/_services/navbar-show-hide.service';

@Component({
  selector: 'app-new-incidents',
  templateUrl: './new-incidents.component.html',
  styleUrls: ['./new-incidents.component.css']
})
export class NewIncidentsComponent implements OnInit {

  constructor(private nav:NavbarShowHideService) { 
    this.nav.show();
  }

  ngOnInit(): void {
  }

}
