import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IncidentsComponent } from './Components/incidents/incidents.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterialExampleModule } from "./../../material.module";
import { MatNativeDateModule } from '@angular/material/core';
import { NewIncidentsComponent } from './Components/new-incidents/new-incidents.component';

const routes: Routes= [
  {
    path:'',
    component:IncidentsComponent
  },
  {
    path:'new',
    component:NewIncidentsComponent
  }
]

@NgModule({
  declarations: [
    IncidentsComponent,
    NewIncidentsComponent
  ],
  imports: [
    CommonModule,
    [RouterModule.forChild(routes)],
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialExampleModule,
    MatNativeDateModule
  ]
})
export class IncidentsModule { }
